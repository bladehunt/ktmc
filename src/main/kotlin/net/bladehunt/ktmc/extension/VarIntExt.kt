package net.bladehunt.ktmc.extension

import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.core.*

fun BytePacketBuilder.writeVarInt(value: Int) {
    when {
        value.toLong() and (0xFFFFFFFF shl 7) == 0L -> writeByte(value.toByte())
        value.toLong() and (0xFFFFFFFF shl 14) == 0L ->
            writeShort((value and 0x7F or 0x80 shl 8 or (value ushr 7)).toShort())

        value.toLong() and (0xFFFFFFFF shl 21) == 0L -> {
            writeByte((value and 0x7F or 0x80).toByte())
            writeByte(((value shr 7) and 0x7F or 0x80).toByte())
            writeByte((value shr 14).toByte())
        }

        value.toLong() and (0xFFFFFFFF shl 28) == 0L ->
            writeInt(
                value and
                    0x7F or
                    0x80 shl
                    24 or
                    (value ushr 7 and 0x7F or 0x80 shl 16) or
                    (value ushr 14 and 0x7F or 0x80 shl 8) or
                    (value ushr 21))

        else -> {
            writeInt(
                value and
                    0x7F or
                    0x80 shl
                    24 or
                    (value ushr 7 and 0x7F or 0x80 shl 16) or
                    (value shr 14 and 0x7F or 0x80 shl 8) or
                    (value ushr 21 and 0x7F or 0x80))
            writeByte((value ushr 28).toByte())
        }
    }
}

fun ByteReadPacket.readVarInt(): Int {
    var result = 0
    var shift = 0

    while (true) {
        val next = readByte()
        result = result or ((next.toInt() and 0x7F) shl shift)
        if (next >= 0) return result
        shift += 7
    }
}

suspend fun ByteReadChannel.readVarInt(): Int {
    var result = 0
    var shift = 0

    while (true) {
        val next = readByte()
        result = result or ((next.toInt() and 0x7F) shl shift)
        if (next >= 0) return result
        shift += 7
    }
}
