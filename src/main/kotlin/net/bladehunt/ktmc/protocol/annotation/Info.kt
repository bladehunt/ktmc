package net.bladehunt.ktmc.protocol.annotation

import net.bladehunt.ktmc.network.PlayerConnection

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Info(val id: Int, vararg val states: PlayerConnection.State)
