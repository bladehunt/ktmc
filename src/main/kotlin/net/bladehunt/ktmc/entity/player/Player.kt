package net.bladehunt.ktmc.entity.player

import net.bladehunt.ktmc.network.PlayerConnection
import java.util.*

class Player(val connection: PlayerConnection, uuid: UUID, username: String) {}
