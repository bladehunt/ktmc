package net.bladehunt.ktmc.protocol.clientbound.status

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.protocol.annotation.Info

@Serializable @Info(0x00) data class PingResponsePacket(val payload: Long)
