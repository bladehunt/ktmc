package net.bladehunt.ktmc.protocol.serverbound.login

import java.util.*
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.clientbound.login.LoginSuccessPacket
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x00, PlayerConnection.State.LOGIN)
data class LoginStartPacket(val name: String, val uuid: @Contextual UUID) : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {

        connection.uuid = uuid
        connection.username = name
        connection.writePacket(LoginSuccessPacket(uuid, name, 0, false))
    }
}
