package net.bladehunt.ktmc.protocol.clientbound.status

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.annotation.JsonEncoded
import net.bladehunt.ktmc.protocol.clientbound.Clientbound

@Serializable
@Info(0x00)
@OptIn(ExperimentalSerializationApi::class)
data class StatusResponsePacket(@JsonEncoded val response: Response) : Clientbound {
    @Serializable
    data class Response(
        val version: Version,
        val players: Players? = null,
        val description: String? = null,
        val favicon: String? = null,
        val enforcesSecureChat: Boolean = false,
        val previewsChat: Boolean = false
    ) {
        @Serializable data class Version(val name: String, val protocol: Int)

        @Serializable
        data class Players(val max: Int, val online: Int, val sample: List<Entry>? = null) {
            @Serializable data class Entry(val name: String, val id: String)
        }
    }
}
