package comet.protocol.serialization.serializer

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.util.*

object UuidSerializer : KSerializer<UUID> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("uuid", PrimitiveKind.LONG)

    override fun deserialize(decoder: Decoder): UUID {
        val mostSignificantBits = decoder.decodeLong()
        val leastSignificantBits = decoder.decodeLong()

        return UUID(mostSignificantBits, leastSignificantBits)
    }

    override fun serialize(encoder: Encoder, value: UUID) {
        encoder.encodeLong(value.mostSignificantBits)
        encoder.encodeLong(value.leastSignificantBits)
    }
}
