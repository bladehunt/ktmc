package net.bladehunt.ktmc

import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import net.bladehunt.ktmc.entity.player.Player
import net.bladehunt.ktmc.exception.ConnectionUpgradeException
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.network.PlayerConnectionManager
import org.slf4j.LoggerFactory

class Server {
    val logger = LoggerFactory.getLogger(this::class.java)
    val playerConnectionManager: PlayerConnectionManager = PlayerConnectionManager()

    suspend fun start(bind: String = "127.0.0.1", port: Int = 25565) {
        val server =
            aSocket(ActorSelectorManager(Dispatchers.Default))
                .tcp()
                .bind(InetSocketAddress(bind, port))

        logger.info("Server started on ${server.localAddress}")
        coroutineScope {
            while (!server.isClosed) {
                val socket = server.accept()
                val playerConnection =
                    object : PlayerConnection(socket) {
                        override fun upgrade(): Player {
                            when (state.get()!!) {
                                State.HANDSHAKING,
                                State.STATUS,
                                State.LOGIN -> {
                                    throw ConnectionUpgradeException(
                                        "Cannot upgrade connection when not in CONFIGURATION or PLAY"
                                    )
                                }
                                State.CONFIGURATION,
                                State.PLAY -> {
                                    return playerConnectionManager.getPlayer(this)
                                        ?: playerConnectionManager.initPlayer(this)
                                }
                            }
                        }
                    }
                launch(CoroutineName("session/${playerConnection.remoteAddress}")) {
                    logger.info(
                        "Handling incoming connection from ${playerConnection.remoteAddress}"
                    )
                    playerConnection.handleIncoming()
                    logger.info(
                        "Finished handling connection from ${playerConnection.remoteAddress}"
                    )
                }
            }
        }
    }
}
