package net.bladehunt.ktmc.entity.player

import net.bladehunt.ktmc.network.PlayerConnection
import java.util.*

fun interface PlayerFactory {
    fun createPlayer(connection: PlayerConnection, uuid: UUID, username: String): Player
}
