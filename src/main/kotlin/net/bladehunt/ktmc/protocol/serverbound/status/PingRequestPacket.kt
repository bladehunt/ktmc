package net.bladehunt.ktmc.protocol.serverbound.status

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.clientbound.status.PingResponsePacket
import net.bladehunt.ktmc.protocol.serialization.Packet
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x01, PlayerConnection.State.STATUS)
data class PingRequestPacket(val payload: Long) : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        connection.writePacket(Packet.encodeToPacket(PingResponsePacket(payload)))
        connection.close()
    }
}
