package net.bladehunt.ktmc.protocol.serverbound

import net.bladehunt.ktmc.network.PlayerConnection

interface Serverbound {
    suspend fun handle(connection: PlayerConnection)
}
