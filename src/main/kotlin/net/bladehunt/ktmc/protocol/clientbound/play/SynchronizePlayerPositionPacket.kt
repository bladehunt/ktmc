package net.bladehunt.ktmc.protocol.clientbound.play

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.annotation.VariableLength

@Serializable
@Info(0x40)
data class SynchronizePlayerPositionPacket(
    val x: Double,
    val y: Double,
    val z: Double,
    val yaw: Float,
    val pitch: Float,
    val flags: Byte,
    @VariableLength val teleportId: Int
)
