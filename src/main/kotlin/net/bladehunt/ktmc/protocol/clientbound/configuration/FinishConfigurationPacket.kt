package net.bladehunt.ktmc.protocol.clientbound.configuration

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.clientbound.Clientbound

@Serializable @Info(0x03) data object FinishConfigurationPacket : Clientbound
