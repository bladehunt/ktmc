package net.bladehunt.ktmc.protocol

import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.serializer
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.serverbound.Serverbound
import net.bladehunt.ktmc.protocol.serverbound.configuration.ClientInformationPacket
import net.bladehunt.ktmc.protocol.serverbound.configuration.FinishConfigurationPacket
import net.bladehunt.ktmc.protocol.serverbound.configuration.PluginMessagePacket
import net.bladehunt.ktmc.protocol.serverbound.handshake.HandshakePacket
import net.bladehunt.ktmc.protocol.serverbound.login.LoginAcknowledgedPacket
import net.bladehunt.ktmc.protocol.serverbound.login.LoginStartPacket
import net.bladehunt.ktmc.protocol.serverbound.status.PingRequestPacket
import net.bladehunt.ktmc.protocol.serverbound.status.StatusRequestPacket
import org.slf4j.LoggerFactory
import kotlin.reflect.full.findAnnotation

object Protocol {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val serverboundPackets:
        MutableMap<PlayerConnection.State, MutableMap<Int, KSerializer<out Serverbound>>> =
        hashMapOf()

    const val VERSION: Int = 766

    init {
        registerServerboundPacket<HandshakePacket>()

        // Status
        registerServerboundPacket<PingRequestPacket>()
        registerServerboundPacket<StatusRequestPacket>()

        // Login
        registerServerboundPacket<LoginStartPacket>()
        registerServerboundPacket<LoginAcknowledgedPacket>()

        // Configuration
        registerServerboundPacket<ClientInformationPacket>()
        registerServerboundPacket<PluginMessagePacket>()
        registerServerboundPacket<FinishConfigurationPacket>()
    }

    @OptIn(InternalSerializationApi::class)
    private inline fun <reified T : Serverbound> registerServerboundPacket() {
        val info =
            T::class.findAnnotation<Info>()
                ?: run {
                    logger.warn("Failed to find packet info for ${T::class.simpleName}")
                    return
                }

        val serializer = T::class.serializer()

        info.states.forEach { state ->
            serverboundPackets.getOrPut(state) { mutableMapOf() }[info.id] = serializer
        }
    }

    fun getSerializer(
        state: PlayerConnection.State,
        packetId: Int
    ): KSerializer<out Serverbound>? {
        return serverboundPackets[state]?.get(packetId)
    }
}
