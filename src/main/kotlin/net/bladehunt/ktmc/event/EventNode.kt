package net.bladehunt.ktmc.event

class EventNode<T : Event>(
    private var parent: EventNode<in T>? = null,
    private val children: MutableList<EventNode<out T>> = arrayListOf(),
    private val listeners: MutableList<Listener<T>> = arrayListOf()
) {
    fun dispatch(event: T) {
        listeners.filter { event::class.java == it.first }.forEach { it.second(event) }
    }

    inline fun <reified E : T> listen(noinline listener: (E) -> Unit) =
        listen(E::class.java, listener)

    fun <E : T> listen(clazz: Class<E>, listener: (E) -> Unit) {
        listeners += clazz to (listener as (T) -> Unit)
    }
}
