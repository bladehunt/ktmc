package net.bladehunt.ktmc.protocol.serverbound.configuration

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x02, PlayerConnection.State.CONFIGURATION)
data class PluginMessagePacket(val channel: String, val data: ByteArray) : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        TODO("Not yet implemented")
    }
}
