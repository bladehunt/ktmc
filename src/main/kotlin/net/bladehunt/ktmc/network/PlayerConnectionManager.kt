package net.bladehunt.ktmc.network

import net.bladehunt.ktmc.entity.player.Player
import net.bladehunt.ktmc.entity.player.PlayerFactory
import java.util.*

class PlayerConnectionManager {
    private val players = mutableMapOf<UUID, Player>()
    var playerFactory: PlayerFactory = PlayerFactory(::Player)

    fun getPlayer(uuid: UUID): Player? = players[uuid]

    fun getPlayer(connection: PlayerConnection): Player? =
        players.values.firstOrNull { it.connection == connection }

    fun initPlayer(connection: PlayerConnection): Player {
        val player = playerFactory.createPlayer(connection, connection.uuid, connection.username)
        players[connection.uuid]?.connection?.close()
        players[connection.uuid] = player
        return player
    }
}
