package net.bladehunt.ktmc.protocol.annotation

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialInfo

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@ExperimentalSerializationApi
@SerialInfo
annotation class JsonEncoded
