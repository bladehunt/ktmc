package net.bladehunt.ktmc.protocol.serialization

import comet.protocol.serialization.PacketDecoder
import comet.protocol.serialization.PacketEncoder
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.core.*
import io.ktor.utils.io.streams.inputStream
import kotlin.reflect.full.findAnnotation
import kotlin.text.toByteArray
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.EmptySerializersModule
import kotlinx.serialization.modules.SerializersModule
import net.benwoodworth.knbt.Nbt
import net.benwoodworth.knbt.NbtCompression
import net.benwoodworth.knbt.NbtVariant
import net.benwoodworth.knbt.decodeFromStream
import net.bladehunt.ktmc.extension.readVarInt
import net.bladehunt.ktmc.extension.writeVarInt
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.Protocol
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

sealed class Packet(
    val serializersModule: SerializersModule = EmptySerializersModule(),
    private val json: Json = Json,
    private val nbt: Nbt = Nbt {
        variant = NbtVariant.Java
        compression = NbtCompression.None
        nameRootClasses = false
    }
) {
    fun <T : Any> encodeToPacket(
        serializer: KSerializer<T>,
        packet: T,
        builder: BytePacketBuilder
    ) {
        val info =
            packet::class.findAnnotation<Info>()
                ?: throw RuntimeException("Couldn't find packet info")

        val packet = buildPacket {
            writeVarInt(info.id)
            PacketEncoderImpl(this).encodeSerializableValue(serializer, packet)
        }

        builder.writeVarInt(packet.remaining.toInt())

        builder.writePacket(packet)
    }

    @OptIn(InternalSerializationApi::class)
    inline fun <reified T : Any> encodeToPacket(packet: T, builder: BytePacketBuilder) =
        encodeToPacket(T::class.serializer(), packet, builder)

    @OptIn(InternalSerializationApi::class)
    inline fun <reified T : Any> encodeToPacket(packet: T): ByteReadPacket = buildPacket {
        encodeToPacket(T::class.serializer(), packet, this)
    }

    suspend fun decodeFromChannel(
        state: PlayerConnection.State,
        channel: ByteReadChannel
    ): Serverbound {
        val len = channel.readVarInt()
        val data = channel.readPacket(len)

        return decodeFromPacket(state, data) as Serverbound
    }

    fun decodeFromPacket(state: PlayerConnection.State, packet: ByteReadPacket): Serverbound {
        val decoder = PacketDecoderImpl(packet)
        val id = decoder.decodeVarInt()

        val serializer =
            Protocol.getSerializer(state, id)
                ?: throw RuntimeException(
                    "Couldn't find a serializer for packet $id in state $state")

        return decoder.decodeSerializableValue(serializer) as Serverbound
    }

    companion object Default : Packet()

    internal inner class PacketEncoderImpl(private val builder: BytePacketBuilder) :
        PacketEncoder() {
        override val serializersModule: SerializersModule
            get() = this@Packet.serializersModule

        override fun encodeBoolean(value: Boolean) {
            builder.writeByte(if (value) 0x1 else 0x0)
        }

        override fun encodeByte(value: Byte) {
            builder.writeByte(value)
        }

        override fun encodeChar(value: Char) = encodeString(value.toString())

        override fun encodeDouble(value: Double) {
            builder.writeDouble(value)
        }

        override fun encodeEnum(enumDescriptor: SerialDescriptor, index: Int) {
            builder.writeInt(index)
        }

        override fun encodeFloat(value: Float) {
            builder.writeFloat(value)
        }

        override fun encodeInline(descriptor: SerialDescriptor): Encoder = this

        override fun encodeInt(value: Int) {
            builder.writeInt(value)
        }

        override fun encodeLong(value: Long) {
            builder.writeLong(value)
        }

        override fun encodeShort(value: Short) {
            builder.writeShort(value)
        }

        override fun encodeString(value: String) {
            builder.writeVarInt(value.length)
            builder.writeFully(value.toByteArray())
        }

        override fun encodeVarInt(int: Int) {
            builder.writeVarInt(int)
        }

        override fun encodeVarLong(long: Long) {
            TODO("Not yet implemented")
        }

        override fun <T> encodeAsNbt(
            descriptor: SerialDescriptor,
            serializer: SerializationStrategy<T>,
            value: T
        ) = builder.writeFully(nbt.encodeToByteArray(serializer, value))

        override fun <T> encodeAsJson(
            descriptor: SerialDescriptor,
            serializer: SerializationStrategy<T>,
            value: T
        ) = encodeString(json.encodeToString(serializer, value))
    }

    internal inner class PacketDecoderImpl(
        private val packet: ByteReadPacket,
    ) : PacketDecoder() {
        override val serializersModule: SerializersModule
            get() = this@Packet.serializersModule

        override fun decodeVarInt(): Int = packet.readVarInt()

        override fun decodeVarLong(): Long = TODO("VarLong implementation")

        override fun <T> decodeNbtTag(deserializer: DeserializationStrategy<T>): T =
            nbt.decodeFromStream(deserializer, packet.inputStream())

        override fun decodeBoolean(): Boolean = packet.readByte() == 0x1.toByte()

        override fun decodeByte(): Byte = packet.readByte()

        override fun decodeChar(): Char = decodeString()[0]

        override fun decodeDouble(): Double = packet.readDouble()

        override fun decodeEnum(enumDescriptor: SerialDescriptor): Int = packet.readVarInt()

        override fun decodeFloat(): Float = packet.readFloat()

        override fun decodeInline(descriptor: SerialDescriptor): Decoder = this

        override fun decodeInt(): Int = packet.readInt()

        override fun decodeLong(): Long = packet.readLong()

        override fun decodeShort(): Short = packet.readShort()

        override fun decodeString(): String {
            val len = decodeVarInt()

            return packet.readBytes(len).decodeToString()
        }

        private var elementIndex = 0

        @OptIn(ExperimentalSerializationApi::class)
        override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
            if (elementIndex == descriptor.elementsCount) return CompositeDecoder.DECODE_DONE
            return elementIndex++
        }
    }
}
