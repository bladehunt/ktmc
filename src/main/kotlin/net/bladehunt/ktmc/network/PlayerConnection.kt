package net.bladehunt.ktmc.network

import io.ktor.network.sockets.*
import io.ktor.utils.io.*
import io.ktor.utils.io.core.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.isActive
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.serializer
import net.bladehunt.ktmc.entity.player.Player
import net.bladehunt.ktmc.protocol.clientbound.Clientbound
import net.bladehunt.ktmc.protocol.serialization.Packet
import org.slf4j.LoggerFactory
import java.io.Closeable
import java.util.UUID
import java.util.concurrent.atomic.AtomicReference

abstract class PlayerConnection(private val socket: Socket) : Closeable {
    lateinit var uuid: UUID
    lateinit var username: String

    val logger = LoggerFactory.getLogger(this::class.java)
    val state = AtomicReference(State.HANDSHAKING)
    val remoteAddress: SocketAddress
        get() = socket.remoteAddress

    private val incoming = socket.openReadChannel()
    private val outgoing = socket.openWriteChannel(autoFlush = true)

    suspend fun handleIncoming() = coroutineScope {
        use {
            while (isActive) {
                try {
                    receivePacket()
                } catch (_: ClosedReceiveChannelException) {
                    logger.error("Connection closed by peer")
                    break
                } catch (_: CancellationException) {
                    logger.error("Connection cancelled")
                    break
                }
            }
        }
    }

    abstract fun upgrade(): Player

    @Suppress("UNCHECKED_CAST")
    private suspend fun receivePacket() {
        val decodedPacket = Packet.decodeFromChannel(state.get(), incoming)

        decodedPacket.handle(this)
    }

    @OptIn(InternalSerializationApi::class)
    suspend fun <T : Clientbound> writePacket(packet: T) {
        outgoing.writePacket(
            buildPacket {
                Packet.encodeToPacket(packet::class.serializer() as KSerializer<Any>, packet, this)
            })
    }

    suspend fun writePacket(packet: ByteReadPacket) {
        outgoing.writePacket(packet)
    }

    enum class State {
        HANDSHAKING,
        STATUS,
        LOGIN,
        CONFIGURATION,
        PLAY
    }

    override fun close() {
        runCatching { incoming.cancel() }
            .onFailure { logger.warn("Failed to close socket: ${it.message}") }
    }
}
