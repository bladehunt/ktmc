package net.bladehunt.ktmc.protocol.serverbound.status

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.event.connection.ServerListPingEvent
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.Protocol
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.clientbound.status.StatusResponsePacket
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x00, PlayerConnection.State.STATUS)
data object StatusRequestPacket : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        val event =
            ServerListPingEvent(
                StatusResponsePacket.Response(
                    StatusResponsePacket.Response.Version("1.21", Protocol.VERSION),
                    StatusResponsePacket.Response.Players(1, 0),
                    "A KtMc server"))

        connection.server.globalEventNode.dispatch(event)

        connection.writePacket(StatusResponsePacket(event.response))
    }
}
