package net.bladehunt.ktmc.protocol.serverbound.login

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x03, PlayerConnection.State.LOGIN)
data object LoginAcknowledgedPacket : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        connection.state.set(PlayerConnection.State.CONFIGURATION)
    }
}
