package net.bladehunt.ktmc.protocol.clientbound.play

/*
@Serializable
@Packet(0x2B)
data class LoginPlayPacket(
    val entityId: Int,
    val isHardcore: Boolean,
    val dimensions: LengthList<Identifier>,

    val maxPlayers: VarInt,
    val viewDistance: VarInt,
    val simulationDistance: VarInt,
    val reducedDebugInfo: Boolean,
    val enableRespawnScreen: Boolean,
    val doLimitedCrafting: Boolean,
    val dimensionType: VarInt,
    val dimensionName: Identifier,
    val hashedSeed: Long,
    val gameMode: Byte,
    val previousGameMode: Byte,
    val isDebug: Boolean,
    val isFlat: Boolean,
    val hasDeathLocation: Boolean,
    val deathDimensionName: Identifier? = null,
    val deathLocation: VarInt? = null,
    val portalCooldown: VarInt,
    val enforcesSecureChat: Boolean
)
*/