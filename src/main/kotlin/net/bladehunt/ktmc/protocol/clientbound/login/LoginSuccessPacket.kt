package net.bladehunt.ktmc.protocol.clientbound.login

import java.util.*
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.clientbound.Clientbound

@Serializable
@Info(0x02)
data class LoginSuccessPacket(
    val uuid: @Contextual UUID,
    val username: String,
    val properties: Int,
    val strictErrorHandling: Boolean
) : Clientbound
