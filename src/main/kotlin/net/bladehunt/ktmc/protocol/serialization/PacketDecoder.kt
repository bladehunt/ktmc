package comet.protocol.serialization

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.json.Json
import net.bladehunt.ktmc.protocol.annotation.JsonEncoded
import net.bladehunt.ktmc.protocol.annotation.NbtEncoded
import net.bladehunt.ktmc.protocol.annotation.VariableLength

@OptIn(ExperimentalSerializationApi::class)
internal abstract class PacketDecoder : Decoder, CompositeDecoder {
    abstract fun decodeVarInt(): Int

    abstract fun decodeVarLong(): Long

    abstract fun <T> decodeNbtTag(deserializer: DeserializationStrategy<T>): T

    override fun decodeNull(): Nothing? = null

    override fun beginStructure(descriptor: SerialDescriptor): CompositeDecoder = this

    override fun endStructure(descriptor: SerialDescriptor) {}

    final override fun decodeBooleanElement(descriptor: SerialDescriptor, index: Int): Boolean =
        decodeBoolean()

    final override fun decodeByteElement(descriptor: SerialDescriptor, index: Int): Byte =
        decodeByte()

    final override fun decodeShortElement(descriptor: SerialDescriptor, index: Int): Short =
        decodeShort()

    final override fun decodeIntElement(descriptor: SerialDescriptor, index: Int): Int {
        if (descriptor.getElementAnnotations(index).any { it is VariableLength }) {
            return decodeVarInt()
        }
        return decodeInt()
    }

    final override fun decodeLongElement(descriptor: SerialDescriptor, index: Int): Long {
        if (descriptor.getElementAnnotations(index).any { it is VariableLength })
            return decodeVarLong()
        return decodeLong()
    }

    final override fun decodeFloatElement(descriptor: SerialDescriptor, index: Int): Float =
        decodeFloat()

    final override fun decodeDoubleElement(descriptor: SerialDescriptor, index: Int): Double =
        decodeDouble()

    final override fun decodeCharElement(descriptor: SerialDescriptor, index: Int): Char =
        decodeChar()

    final override fun decodeStringElement(descriptor: SerialDescriptor, index: Int): String =
        decodeString()

    override fun decodeInlineElement(descriptor: SerialDescriptor, index: Int): Decoder =
        decodeInline(descriptor.getElementDescriptor(index))

    override fun decodeNotNullMark(): Boolean = true

    override fun <T> decodeSerializableElement(
        descriptor: SerialDescriptor,
        index: Int,
        deserializer: DeserializationStrategy<T>,
        previousValue: T?
    ): T {
        if (descriptor.getElementAnnotations(index).any { it is JsonEncoded }) {
            return Json.decodeFromString(deserializer, decodeString())
        }
        if (descriptor.getElementAnnotations(index).any { it is NbtEncoded }) {
            return decodeNbtTag(deserializer)
        }
        return decodeSerializableValue(deserializer)
    }

    final override fun <T : Any> decodeNullableSerializableElement(
        descriptor: SerialDescriptor,
        index: Int,
        deserializer: DeserializationStrategy<T?>,
        previousValue: T?
    ): T? = decodeIfNullable(deserializer) { decodeSerializableValue(deserializer) }

    @OptIn(ExperimentalSerializationApi::class)
    internal inline fun <T : Any> Decoder.decodeIfNullable(
        deserializer: DeserializationStrategy<T?>,
        block: () -> T?
    ): T? {
        val isNullabilitySupported = deserializer.descriptor.isNullable
        return if (isNullabilitySupported || decodeNotNullMark()) block() else decodeNull()
    }
}
