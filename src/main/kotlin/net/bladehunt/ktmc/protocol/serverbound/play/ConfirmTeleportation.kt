package net.bladehunt.ktmc.protocol.serverbound.play

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.annotation.VariableLength
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x00, PlayerConnection.State.PLAY)
data class ConfirmTeleportation(@VariableLength val teleportId: Int) : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        TODO("Not yet implemented")
    }
}
