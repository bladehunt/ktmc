package net.bladehunt.ktmc.protocol.serverbound.configuration

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.annotation.VariableLength
import net.bladehunt.ktmc.protocol.clientbound.configuration.FinishConfigurationPacket
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x00, PlayerConnection.State.CONFIGURATION)
data class ClientInformationPacket(
    val locale: String,
    val viewDistance: Byte,
    val chatMode: ChatMode,
    val chatColors: Boolean,
    val displayedSkinParts: Byte,
    @VariableLength val mainHand: Int,
    val enableTextFiltering: Boolean,
    val allowServerListings: Boolean
) : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        connection.writePacket(FinishConfigurationPacket)
    }

    enum class ChatMode {
        ENABLED,
        COMMANDS_ONLY,
        HIDDEN
    }
}
