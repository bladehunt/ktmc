package net.bladehunt.ktmc.protocol

import net.bladehunt.ktmc.network.PlayerConnection

fun interface Processor<T> {
    suspend fun processPacket(packet: T, connection: PlayerConnection)
}
