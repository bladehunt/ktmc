package net.bladehunt.ktmc.protocol.serverbound.login

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.annotation.VariableLength
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x02, PlayerConnection.State.LOGIN)
data class LoginPluginResponsePacket(
    @VariableLength val messageId: Int,
    val successful: Boolean,
    val byteArray: ByteArray? = null
) : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        println(this)
    }
}
