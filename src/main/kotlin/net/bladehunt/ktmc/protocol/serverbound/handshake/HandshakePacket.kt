package net.bladehunt.ktmc.protocol.serverbound.handshake

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.annotation.VariableLength
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x00, PlayerConnection.State.HANDSHAKING)
data class HandshakePacket(
    @VariableLength val protocolVersion: Int,
    val address: String,
    val port: Short,
    val nextState: PlayerConnection.State
) : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        connection.logger.debug(
            "Received handshake from {} (Next state: {})", connection.remoteAddress, nextState)
        connection.state.set(nextState)
    }
}
