package net.bladehunt.ktmc.protocol.clientbound

import kotlin.reflect.full.findAnnotation
import net.bladehunt.ktmc.protocol.annotation.Info

interface Clientbound {
    val packetId: Int
        get() = this::class.findAnnotation<Info>()!!.id
}
