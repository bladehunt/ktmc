package net.bladehunt.ktmc.protocol.serverbound.configuration

import kotlinx.serialization.Serializable
import net.bladehunt.ktmc.network.PlayerConnection
import net.bladehunt.ktmc.protocol.annotation.Info
import net.bladehunt.ktmc.protocol.serverbound.Serverbound

@Serializable
@Info(0x03, PlayerConnection.State.CONFIGURATION)
data object FinishConfigurationPacket : Serverbound {
    override suspend fun handle(connection: PlayerConnection) {
        connection.logger.debug("Finished configuration for {}", connection.username)
        connection.state.set(PlayerConnection.State.PLAY)
    }
}
