plugins {
    kotlin("jvm") version "2.0.0"
    kotlin("plugin.serialization") version "2.0.0"
    id("io.github.goooler.shadow") version "8.1.7"
}

group = "net.bladehunt"

version = "0.1.0-alpha.1"

repositories { mavenCentral() }

dependencies {
    testImplementation(kotlin("test"))
    api(kotlin("reflect"))
    api("org.slf4j:slf4j-api:2.0.13")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.7.0-RC")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.7.0-RC")
    implementation("net.benwoodworth.knbt:knbt:0.11.5")
    implementation("io.ktor:ktor-server-netty:2.3.11")
    implementation("io.ktor:ktor-network:2.3.11")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.9.0-RC")
}

tasks.test { useJUnitPlatform() }

kotlin { jvmToolchain(21) }
