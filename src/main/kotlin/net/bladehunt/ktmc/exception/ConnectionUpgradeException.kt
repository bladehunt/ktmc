package net.bladehunt.ktmc.exception

class ConnectionUpgradeException(
    override val message: String? = null,
    override val cause: Throwable? = null
) : Exception()
