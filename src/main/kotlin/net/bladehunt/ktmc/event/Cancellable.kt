package net.bladehunt.ktmc.event

interface Cancellable {
    var isCancelled: Boolean
}
