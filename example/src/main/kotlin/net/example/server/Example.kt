package net.example.server

import kotlinx.coroutines.coroutineScope
import net.bladehunt.ktmc.Server
import net.bladehunt.ktmc.event.connection.ServerListPingEvent

suspend fun main() = coroutineScope {
    val server = Server()
    server.globalEventNode.listen<ServerListPingEvent> {
        it.response = it.response.copy(description = "HELLO!!!")
    }
    server.start()
}
