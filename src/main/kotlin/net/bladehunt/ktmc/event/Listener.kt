package net.bladehunt.ktmc.event

typealias Listener<T> = Pair<Class<out T>, (T) -> Unit>
