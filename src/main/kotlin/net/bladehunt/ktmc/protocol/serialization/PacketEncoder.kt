package comet.protocol.serialization

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.CompositeEncoder
import kotlinx.serialization.encoding.Encoder
import net.bladehunt.ktmc.protocol.annotation.JsonEncoded
import net.bladehunt.ktmc.protocol.annotation.NbtEncoded
import net.bladehunt.ktmc.protocol.annotation.VariableLength
import net.bladehunt.ktmc.protocol.serialization.NoOpEncoder

@OptIn(ExperimentalSerializationApi::class)
internal abstract class PacketEncoder : CompositeEncoder, Encoder {
    abstract fun encodeVarInt(int: Int)

    abstract fun encodeVarLong(long: Long)

    abstract fun <T : Any?> encodeAsNbt(
        descriptor: SerialDescriptor,
        serializer: SerializationStrategy<T>,
        value: T
    )

    abstract fun <T : Any?> encodeAsJson(
        descriptor: SerialDescriptor,
        serializer: SerializationStrategy<T>,
        value: T
    )

    override fun encodeNull() {}

    override fun endStructure(descriptor: SerialDescriptor) {}

    override fun beginStructure(descriptor: SerialDescriptor): CompositeEncoder = this

    /**
     * Invoked before writing an element that is part of the structure to determine whether it
     * should be encoded. Element information can be obtained from the [descriptor] by the given
     * [index].
     *
     * @return `true` if the value should be encoded, false otherwise
     */
    open fun encodeElement(descriptor: SerialDescriptor, index: Int): Boolean = true

    // Delegating implementation of CompositeEncoder
    final override fun encodeBooleanElement(
        descriptor: SerialDescriptor,
        index: Int,
        value: Boolean
    ) {
        if (encodeElement(descriptor, index)) encodeBoolean(value)
    }

    final override fun encodeByteElement(descriptor: SerialDescriptor, index: Int, value: Byte) {
        if (encodeElement(descriptor, index)) encodeByte(value)
    }

    final override fun encodeShortElement(descriptor: SerialDescriptor, index: Int, value: Short) {
        if (encodeElement(descriptor, index)) encodeShort(value)
    }

    final override fun encodeIntElement(descriptor: SerialDescriptor, index: Int, value: Int) {
        if (encodeElement(descriptor, index)) {
            if (descriptor.getElementAnnotations(index).any { it is VariableLength })
                encodeVarInt(value)
            else encodeInt(value)
        }
    }

    final override fun encodeLongElement(descriptor: SerialDescriptor, index: Int, value: Long) {
        if (encodeElement(descriptor, index)) {
            if (descriptor.getElementAnnotations(index).any { it is VariableLength })
                encodeVarLong(value)
            else encodeLong(value)
        }
    }

    final override fun encodeFloatElement(descriptor: SerialDescriptor, index: Int, value: Float) {
        if (encodeElement(descriptor, index)) encodeFloat(value)
    }

    final override fun encodeDoubleElement(
        descriptor: SerialDescriptor,
        index: Int,
        value: Double
    ) {
        if (encodeElement(descriptor, index)) encodeDouble(value)
    }

    final override fun encodeCharElement(descriptor: SerialDescriptor, index: Int, value: Char) {
        if (encodeElement(descriptor, index)) encodeChar(value)
    }

    final override fun encodeStringElement(
        descriptor: SerialDescriptor,
        index: Int,
        value: String
    ) {
        if (encodeElement(descriptor, index)) encodeString(value)
    }

    final override fun encodeInlineElement(descriptor: SerialDescriptor, index: Int): Encoder =
        if (encodeElement(descriptor, index)) encodeInline(descriptor.getElementDescriptor(index))
        else NoOpEncoder

    override fun <T : Any?> encodeSerializableElement(
        descriptor: SerialDescriptor,
        index: Int,
        serializer: SerializationStrategy<T>,
        value: T
    ) {
        if (!encodeElement(descriptor, index)) return

        if (descriptor.getElementAnnotations(index).any { it is JsonEncoded }) {
            encodeAsJson(descriptor, serializer, value)
            return
        }
        if (descriptor.getElementAnnotations(index).any { it is NbtEncoded }) {
            encodeAsNbt(descriptor, serializer, value)
            return
        }
        encodeSerializableValue(serializer, value)
    }

    override fun <T : Any> encodeNullableSerializableElement(
        descriptor: SerialDescriptor,
        index: Int,
        serializer: SerializationStrategy<T>,
        value: T?
    ) {
        if (encodeElement(descriptor, index)) encodeNullableSerializableValue(serializer, value)
    }
}
