plugins { kotlin("jvm") }

repositories { mavenCentral() }

dependencies {
    implementation(project(":"))
    implementation("ch.qos.logback:logback-classic:1.5.6")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.9.0-RC")
}
